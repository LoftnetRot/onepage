const list = [
  {
    name: '张三',
    age: 18,
    birthday: '20200520',
    address: '天上'
},
{
    name: '李四',
    age: 25,
    birthday: '20210520',
    address: '地下'
},
{
    name: '王五',
    age: 30,
    birthday: '20200519',
    address: '水里'
},
{
    name: '赵六',
    age: 26,
    birthday: '20220612',
    address: '地球'
}
]

module.exports = [
  {
    url: '/vue-admin-template/test/list',
    type: 'get',
    response: config => {
      return {
        code: 20000,
        data: {
          total: list.length,
          items: list
        }
      }
    }
  }
]